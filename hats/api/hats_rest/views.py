from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from django.http import JsonResponse
import json
from .models import Hat, LocationVO


# Create your views here.
class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "closet_name",
        "section_number",
        "shelf_number",
    ]



class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "style_name",
        "color",
        "picture_url",
        "fabric",
        "location"
    ]
    encoders = {
        "location": LocationVOEncoder(),
        }

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "style_name",
        "color",
        "picture_url",
        "fabric",
        "location"
    ]
    encoders = {
        "location": LocationVOEncoder(),
        }



@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder = HatListEncoder,
            safe=False
            )
    else:
        content = json.loads(request.body)
        location_href = content["location"]
        print(location_href)
        location = LocationVO.objects.get(import_href=location_href)
        content["location"] = location
        hat = Hat.objects.create(**content)
        return JsonResponse(
                hat,
                encoder=HatDetailEncoder,
                safe=False
                )

@require_http_methods(["DELETE"])
def api_delete_hats(request, pk):
    if request.method == "DELETE":
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({
            "deleted": count > 0
        })
