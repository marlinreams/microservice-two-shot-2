import React from 'react';

const initialState ={
    style: '',
    color: '',
    fabric: '',
    picture_url: '',
    locations: []
}
class HatsForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = initialState


        // {
        //     style: '',
        //     color: '',
        //     fabric: '',
        //     picture_url: '',
        //     locations: []
        // }



        // this.handleColorChange = this.handleColorChange.bind(this);
        // this.handleStyleChange = this.handleStyleChange.bind(this);
        // this.handleFabricChange = this.handleFabricChange.bind(this);
        // this.handleLocationChange = this.handleLocationChange.bind(this)
        // this.handleSubmit = this.handleSubmit.bind(this)
    }
    handleSubmit= async(event) => {
        event.preventDefault();
        // const data = {
        //     style : this.state.style,
        //     picture_url : this.state.picture_url
        // }
        const data = { ...this.state };
        data.style_name = data.style;
        delete data.style;
        delete data.locations;
        console.log(data);

        const locationUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat);


        }
        // const cleared = {
        //     style: '',
        //     color: '',
        //     fabric: '',
        //     picture_url: '',
        //     location: '',
        // };
        this.setState(initialState);
    }

    async componentDidMount() {
        const url = "http://localhost:8100/api/locations/"

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({ locations: data.locations })

        }
    }

    handleColorChange = (event) => {
        const value = event.target.value;
        this.setState({ color: value })
    }
    handleStyleChange = (event) => {
        const value = event.target.value;
        this.setState({ style: value })
    }
    handleFabricChange = (event) => {
        const value = event.target.value;
        this.setState({ fabric: value })
    }
    handlePictureChange = (event) => {
        const value = event.target.value;
        this.setState({ picture_url: value})
    }
    handleLocationChange = (event) => {
        const value = event.target.value;
        this.setState({ location: value })
    }






    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a Hat</h1>
                        <form id="create-a-hat-form" onSubmit={this.handleSubmit}>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleStyleChange} value={this.state.style} placeholder="Style" required type="text" name="style" id="style" className="form-control" />
                                <label htmlFor="name">Style</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleColorChange} value={this.state.color} placeholder="Color" required type="test" name="color" id="color" className="form-control" />
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleFabricChange} value={this.state.fabric} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                                <label htmlFor="fabric">Fabric</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handlePictureChange} value={this.state.picture_url} placeholder="Picture" required type="text" name="picture" id="picture" className="form-control" />
                                <label htmlFor="fabric">Picture</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleLocationChange} value={this.state.location} name="location" id="location" className="form-select" required>
                                    <option value="">Location</option>
                                    {this.state.locations.map(location => {

                                        return (
                                            <option key={location.href} value={location.href}>{location.closet_name}</option>
                                        )
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default HatsForm;
