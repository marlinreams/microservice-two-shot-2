import React from 'react';

// const loadHats =async () => {
//     const response = await fetch('http://localhost:8090/api/hats/');
//     if (response.ok) {
//       const data = await response.json();
//       root.render(
//         hats={data.hats}
//         );
//     } else {
//       console.error(response);
//     }
//   }
//   loadHats();

async function DeleteHat(id){
    const url = `http://localhost:8090/api/hats/${id}`
    const fetchConfig = {
      method: "DELETE",

    }
    const response = await fetch(url, fetchConfig)
    if (response.ok) {
      window.location.reload(false)
    } else {
      console.error(response)
    }
  }

function HatsList(props) {
    return (
        <table className="table table-row">
            <thead>
                <tr>
                    <th>Image</th>
                    <th>Style</th>
                    <th>Color</th>
                    <th>Fabric</th>
                </tr>
            </thead>
            <tbody>
                {props.hats.map(hats => {
                    return (
                        <tr key={hats.id}>
                            <td><img style={{ width: 100, height: 100}} src={hats.picture_url}></img></td>
                            <td>{hats.style_name}</td>
                            <td>{hats.color}</td>
                            <td>{hats.fabric}</td>
                            <td><button type="button" className="btn btn-outline-danger btn-sm" onClick={() => DeleteHat(hats.id)}>Delete</button></td>
                        </tr>
                    )
                })}

            </tbody>
        </table>
    )
}

export default HatsList;
